// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/#configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    static: '/',
    src: '/_dist_'
  },
  // plugins: [],
  // installOptions: {},
  devOptions: {
    fallback: 'index.html'
  },
  // buildOptions: {},
};
