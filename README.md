# Vanilla Todo

The purpose of this project is to show how much raw JS code goes into creating even a simple application. This should hopefully help people who are new to the web front-end space understand why there are libraries/frameworks dedicated to displaying data in Javascreipt.

Please note this is not a "best practices", and that a few decisions could be marred by the fact that even though I am trying to write this code like a relative beginner, I am definitely _not_ a beginner.

## Branches

The branches of this project will show, what I think, are the same application in various stages of the developer's experience. The mainline branch will be the absolute most basic version, and other branches will be added as time goes on.

### [Mainline](https://gitlab.com/jhechtf/vanilla-todo/-/tree/mainline)

This will be the baseline branch; I do not have a degree in Computer Science, but from what I have gathered from people who do they have at least basic knowledge of something like caching (thus the `itemCache`), and will therefore also have knowledge about Node tree structures. I imagine my particular implementation of the `Map` object could be figured out via some Googling / raw experimentation. This is meant to be "version 1"

### [Version 1.1](https://gitlab.com/jhechtf/vanilla-todo/-/tree/version-1.1)

This branch is meant to be "I've been working in this project for awhile now, and I would like to refactor it to do some things differently."

The things that are different here

1. The application now does not all live inside the `main.js` file. Instead all of the functionality has been put into it's own files, with the main.js file simply serving as the aggregator of those events.
2. The renderer and the store have been largely separated. The render function here is what would be called a "Pure" function -- it keeps no track of state and will render the todos based solely off their values into the DOM.
3. Largely there are no direct interactions with the `todo` variable outside of the `todo.store.js` file with 1 exception. I could imagine that a slightly more knowledgeable engineer would  want to persist information between browser refreshes, and as such takes advantage of the `localStorage`. However, trying to pull this data from storage and use our todos store API was likely a bit more complex. I imagine someone having had a bit of time to fiddle around with adding the items in a loop, but "this way was faster and just worked" is definitely something I could see myself in my earlier years saying.

However, this definitely has some downsides. The largest thing is that since we are no longer doing this all in the `main.js` file there are multiple places where we need to call `document.querySelector('#todo-list-container')`, which even for a relatively small app was rather annoying to write. 

The question then is: how do we make it so that we only have to call the query selector once? There are a few ways, if you are starting off with Javascript and ESM think of a few and test them out.

Beyond that, I think a lot of developers after looking at this code will say something like "I just _feel_ like there should be a way to get the rendering function to run when the data updates... there has to be **some** way to do that, right?"

Also at this point many people are probably feeling the itch of wanting to make the UI a bit prettier, with a navbar and things...

### [Version 1.2](https://gitlab.com/jhechtf/vanilla-todo/-/tree/version-1.2)

This branch is the version of the application where the developer has started more work into finding a solution where directly modifying the todo values links those changes to the DOM.

For this version of the project I've chosen a very rough implementation of the Observer pattern, as I think that of the ways that frameworks and updates function currently that this is probably the easiest to understand; there's not a lot of magic that goes on, like you would have to make the jump to understand with things like Proxies (which I believe Vue uses, though I could be wrong) or the magic that comes from the Svelte compiler. You create a store, and in one particular place that we subscribe to it we set the render function.

This is also nice because it's a relatively simple pattern to follow and extend, meaning that if we should want to create other "components" there really isn't a lot we would need to add. It's also nice because decoupling the store (or "state") from the rendering process means that we could just subscribe to the todos state in other locations to make some additional functionality, such as adding in a "yet to be done" section in an entirely different part of the page (like the navbar).


## Viewing the application locally

**You will need the NodeJS runtime installed on your computer to view this locally**

I do not have this hosted anywhere currently, so you should clone (or fork+clone) this 
repository. 

(*Note:* the `$` below represents a the fact that you are in a terminal shell (such as powershell or just a terminal))

e.g. 
```sh
$ git clone https://gitlab.com/jhechtf/vanilla-todo.git
$ cd vanilla-todo
# I use PNPM for reasons, but you can use the npm version of this:
$ npm install #pnpm install if you also use it; yarn install if you prefer yarn still.
$ npm run dev # pnpm dev
```

This should open a browser window that has the application up and running. It is intentionally very bland -- styling and what not the concern of the application, and only the most minimal CSS has been included.