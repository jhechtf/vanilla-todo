import styles from './styles.module.css';
/**
 * @typedef todo
 * @property {string} title
 * @property {boolean} done
 * @property {string} description
 */

/**
 * @type {todo[]}
 */
const todos = [
  {
    title: 'Example Todo, no details',
    done: false,
    description: ''
  },
  {
    title: 'Example todo, with details',
    done: false,
    description: 'Many details'
  },
  {
    title: 'Completed Todo',
    done: true,
    description: 'with details'
  }
];

/** @type {Map<todo, Element | Element[]} */
const itemCache = new Map();

window.addEventListener('DOMContentLoaded', () => {
  const todoList = document.querySelector('#todo-list-container');
  // add the click listener here so that we don't run into issues later on.
  todoList.addEventListener('click', e => {
    const { target } = e;
    // if our target matches a delete element
    if (target.matches('span[role="delete"]')) {
      e.stopPropagation();
      // grab the key
      const key = target.parentNode.dataset.key;
      // grab the element
      const todo = todos[key];
      // grab the elements from our cahce
      const els = itemCache.get(todo);
      // sanity check
      if (els) {
        // for each element associated with this todo, delete it.
        els.forEach(el => el && el.remove && el.remove());
        // remove the todo from our cache
        itemCache.delete(todo);
        // remove the todo from the array
        todos.splice(key, 1);
        // re-render, mostly to fix key-indexes
        renderTodos(todoList, todos);
      }
    }

    // if our target matches just the regular dt element
    if (target.matches('dt.' + styles['todo-list-item'])) {
      e.stopPropagation();
      // get the key
      const key = target.dataset.key;
      // if the todos does not have this key, stop execution.
      if (!todos[key]) return;
      // invert the value
      todos[key].done = !todos[key].done;
      // re-render
      renderTodos(todoList, todos);
    }
  });

  // add an event handler to the form to prevent default stuff.
  document.querySelector('#todo-form')
    .addEventListener('submit', function (e) {
      // prevent the default
      e.preventDefault();
      // get the elements from this form.
      const { "add-input": addInput, "description-input": descriptionInput } = this.elements;
      // break execution if we do not have strings.
      if (addInput.value.trim().length === 0 || descriptionInput.value.trim().length === 0) return;
      // push the results
      todos.unshift({
        title: addInput.value,
        done: false,
        description: descriptionInput.value
      });

      // Reset the values
      addInput.value = descriptionInput.value = '';

      // Render the todos
      renderTodos(todoList, todos);
    });

  // Render the todos;
  renderTodos(todoList, todos);
});


/**
 * @param {HTMLDListElement} todo
 * @param {todo[]} todos 
 * @returns {void} 
 */
function renderTodos(todo, todos) {
  const els = todos.flatMap((todo, index) => {
    // either we can grab this item from the cache OR we create a new one.
    const [dt, dd] = itemCache.get(todo) || [document.createElement('dt'), document.createElement('dd')];
    if (!itemCache.has(todo)) {
      // set the innerHTML, along with creating the delete span.
      const span = document.createElement('span');
      span.innerHTML = '&times;'
      span.setAttribute('role', 'delete');
      dt.append(
        document.createTextNode(todo.title),
        span
      );

      // Add necessary classes.
      dt.classList.add(styles['todo-list-item']);
      dt.classList.toggle(styles['todo-list-complete'], todo.done);

      //update description element if we have a description
      if (todo.description) {
        dd.innerHTML = todo.description;
      }
      itemCache.set(todo, [dt, dd]);
    }
    dt.dataset.key = dd.dataset.key = index;
    dt.classList.toggle(styles['todo-list-complete'], todo.done);
    return [dt, dd];
  });
  todo.append(...els);
}
